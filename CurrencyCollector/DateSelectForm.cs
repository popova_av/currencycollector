﻿using System;
using System.Windows.Forms;

namespace CurrencyCollector
{
    public partial class DateSelectForm : Form
    {
        /// <summary>
        /// Initializes a new instance of DateSelectForm.
        /// </summary>
        public DateSelectForm()
        {
            InitializeComponent();

            // set current date
            monthNumericUpDown.Value = DateTime.Today.Month;
            dayNumericUpDown.Value = DateTime.Today.Day;
            yearNumericUpDown.Value = DateTime.Today.Year;
        }

        int _day;
        /// <summary>
        /// Gets the selected day.
        /// </summary>
        public int Day
        {
            get
            {
                return _day;
            }
        }

        int _month;
        /// <summary>
        /// Gets the selected month.
        /// </summary>
        public int Month
        {
            get
            {
                return _month;
            }
        }

        int _year;
        /// <summary>
        /// Gets the selected year.
        /// </summary>
        public int Year
        {
            get
            {
                return _year;
            }
        }

        /// <summary>
        /// Saves selected date parameters and closes the form.
        /// </summary>
        private void buttonOk_Click(object sender, EventArgs e)
        {
            _year = (int)yearNumericUpDown.Value;
            _day = (int)dayNumericUpDown.Value;
            _month = (int)monthNumericUpDown.Value;
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
