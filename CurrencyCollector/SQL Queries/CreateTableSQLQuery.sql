USE [CurrencyDB]
GO

CREATE TABLE   
    Currency  
    ( time DATETIME  NOT NULL,
	  euro float NOT NULL,
	  dollar float NOT NULL,
    PRIMARY KEY (time));   