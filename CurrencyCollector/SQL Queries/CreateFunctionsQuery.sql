USE [CurrencyDB]
GO

CREATE FUNCTION dbo.GetEuroByDate(@Date DATE)
  RETURNS TABLE
  AS
  RETURN 
  (
  SELECT AVG(Currency.euro) as Euro FROM Currency WHERE CAST(Currency.time AS DATE) = @Date
  );
  GO

CREATE FUNCTION dbo.GetDollarByDate(@Date DATE)
  RETURNS TABLE
  AS
  RETURN 
  (
  SELECT AVG(Currency.dollar) as Euro FROM Currency WHERE CAST(Currency.time AS DATE) = @Date
  );
  GO