﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace CurrencyCollector
{
    public partial class CurrencyCollectorForm : Form
    {
        /// <summary>
        /// initializes a new instance of CurrencyCollectorForm.
        /// </summary>
        public CurrencyCollectorForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Uploads currency information to database.
        /// </summary>
        private void uploadButtonButton_Click(object sender, EventArgs e)
        {
            double dollar;
            double euro;
            GetEuroDollarFromPage("https://www.cbr.ru/", out dollar, out euro);

            // open sql connection
            using (SqlConnection cn = new SqlConnection())
            {
                cn.ConnectionString = CurrencyCollector.Properties.Settings.Default["ConnectionString"].ToString();
                cn.Open();

                // create insert query
                string query = string.Format(CultureInfo.InvariantCulture,
                        "INSERT INTO dbo.Currency " +
                        "(time,dollar,euro) " +
                        "VALUES (CURRENT_TIMESTAMP, {0}, {1})",
                    dollar,
                    euro);

                // execute sql command
                using (SqlCommand sqlCommand = new SqlCommand(query, cn))
                {
                    sqlCommand.ExecuteNonQuery();
                }

                // close connection
                cn.Close();
            }
        }

        /// <summary>
        /// Gets currency information from the selected url.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="dollar"></param>
        /// <param name="euro"></param>
        public void GetEuroDollarFromPage(string url, out double dollar, out double euro)
        {
            if (!url.Equals("https://www.cbr.ru/"))
                throw new NotImplementedException();

            string line;
            dollar = 0.0;
            euro = 0.0;
            List<string> currencyLines = new List<string>();

            // create http request
            HttpWebRequest request = HttpWebRequest.CreateHttp(url);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                // get response
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    // extract the block which contains currency information
                    while (!stream.EndOfStream)
                    {
                        line = stream.ReadLine();
                        if (line.Contains("Курсы валют"))
                        {
                            while (!line.Contains("</tbody>"))
                            {
                                currencyLines.Add(line);
                                if (stream.EndOfStream)
                                    break;
                                line = stream.ReadLine();
                            }
                            break;
                        }
                    }
                }
            }

            // parce the block with currency information
            ParseCurrencyLines(currencyLines, out dollar, out euro);
            
            if (euro == 0.0 || dollar == 0.0)
                throw new Exception("Currency information not found.");
        }

        /// <summary>
        /// Parses block of string lines which contains currency information.
        /// </summary>
        /// <param name="currencyLines">Block of string lines which contains currency information.</param>
        /// <param name="dollar">Dollar rate.</param>
        /// <param name="euro">Euro rate.</param>
        private void ParseCurrencyLines(List<string> currencyLines, out double dollar, out double euro)
        {
            dollar = 0.0;
            euro = 0.0;
            for (int i = 0; i < currencyLines.Count; i++)
            {
                // parse dollar rate
                string line = currencyLines[i];
                if (line.Contains("Доллар США"))
                {
                    line = currencyLines[i + 3];
                    int startIdx = line.IndexOf(';');
                    int endIdx = line.IndexOf("</td>");
                    dollar = double.Parse(line.Substring(startIdx + 1, endIdx - startIdx - 1));
                }

                // parce euro rate
                if (line.Contains("Евро"))
                {
                    line = currencyLines[i + 3];
                    int startIdx = line.IndexOf(';');
                    int endIdx = line.IndexOf("</td>");
                    euro = double.Parse(line.Substring(startIdx + 1, endIdx - startIdx - 1));
                }
            }
        }

        /// <summary>
        /// Shows date selection form and shows currency information.
        /// </summary>
        private void getCurrencyInfoButton_Click(object sender, EventArgs e)
        {
            int year;
            int month;
            int day;

            // show date selection dialog
            using (DateSelectForm form = new DateSelectForm())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    year = form.Year;
                    month = form.Month;
                    day = form.Day;
                }
                else
                    return;
            }

            // show currency information
            ShowCurrencyInfo(year, month, day);
        }

        /// <summary>
        /// Shows currency information.
        /// </summary>
        /// <param name="year">Selected year.</param>
        /// <param name="month">Selected month.</param>
        /// <param name="day">Selected day.</param>
        private static void ShowCurrencyInfo(int year, int month, int day)
        {
            // open sql connection
            using (SqlConnection cn = new SqlConnection())
            {
                cn.ConnectionString = CurrencyCollector.Properties.Settings.Default["ConnectionString"].ToString();
                cn.Open();

                double dollar = 0.0;
                double euro = 0.0;

                // create select queries
                string query1 = string.Format("SELECT * FROM GetDollarByDate('{0}-{1}-{2}')", year, month, day);
                string query2 = string.Format("SELECT * FROM GetEuroByDate('{0}-{1}-{2}')", year, month, day);

                try
                {
                    // execute sql commands
                    using (SqlCommand sqlCommand = new SqlCommand(query1, cn))
                    {
                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        reader.Read();
                        dollar = reader.GetDouble(0);
                        reader.Close();
                    }
                    using (SqlCommand sqlCommand = new SqlCommand(query2, cn))
                    {
                        SqlDataReader reader = sqlCommand.ExecuteReader();
                        reader.Read();
                        euro = reader.GetDouble(0);
                        reader.Close();
                    }
                }
                catch
                {
                }
                finally
                {
                    // close sql connection
                    cn.Close();

                    if (dollar > 0.0 && euro > 0.0)
                        MessageBox.Show(string.Format("Euro: {0}, dollar: {1}.", euro, dollar));
                    else
                        MessageBox.Show("No information found for the selected date.");
                }
            }
        }
    }
}
