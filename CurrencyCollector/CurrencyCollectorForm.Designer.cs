﻿namespace CurrencyCollector
{
    partial class CurrencyCollectorForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.uploadButton = new System.Windows.Forms.Button();
            this.getCurrencyInfoButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // uploadButton
            // 
            this.uploadButton.Location = new System.Drawing.Point(12, 12);
            this.uploadButton.Name = "uploadButton";
            this.uploadButton.Size = new System.Drawing.Size(226, 23);
            this.uploadButton.TabIndex = 0;
            this.uploadButton.Text = "Upload current currency to database";
            this.uploadButton.UseVisualStyleBackColor = true;
            this.uploadButton.Click += new System.EventHandler(this.uploadButtonButton_Click);
            // 
            // getCurrencyInfoButton
            // 
            this.getCurrencyInfoButton.Location = new System.Drawing.Point(12, 52);
            this.getCurrencyInfoButton.Name = "getCurrencyInfoButton";
            this.getCurrencyInfoButton.Size = new System.Drawing.Size(226, 23);
            this.getCurrencyInfoButton.TabIndex = 1;
            this.getCurrencyInfoButton.Text = "Get currency info from database";
            this.getCurrencyInfoButton.UseVisualStyleBackColor = true;
            this.getCurrencyInfoButton.Click += new System.EventHandler(this.getCurrencyInfoButton_Click);
            // 
            // CurrencyCollectorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(261, 94);
            this.Controls.Add(this.getCurrencyInfoButton);
            this.Controls.Add(this.uploadButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CurrencyCollectorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Currency collector";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button uploadButton;
        private System.Windows.Forms.Button getCurrencyInfoButton;
    }
}

